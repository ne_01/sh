#include <ncurses.h>
#include <readline/readline.h>

#include "ui.h"
#include "luainit.h"

int main (void) {
    if (initlua()) return 3;
    initnc();
    initrl();
    extern unsigned char exitp;
    extern WINDOW *win;
    int c=0;
    while (!exitp) {
        c = wgetch(win);
        switch(c) {
            //case ESC: curs_set(0); break;
            default: curs_set(2); pchar(c); break;
        }
    }
    deinitrl();
    deinitnc();
    deinitlua();
}
