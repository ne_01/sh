#ifndef __LUAINIT__
#define __LUAINIT__

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

extern lua_State *gL;
int interpret (const char *const);
int initlua (void);
int deinitlua (void);

#endif
