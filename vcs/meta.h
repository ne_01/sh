#ifndef __META_
#define __META_

#include "tiger/tiger.h"

#include <time.h>

#define RROOT "./.metadata"
#define OBJPATH RROOT"/objdb/"
#define OBJPATHSIZE (sizeof(OBJPATH) - 1)
#define REFPATH RROOT"/refs/"
#define REFPATHSIZE (sizeof(REFPATH) - 1)
#define INDEXFILE RROOT"/index"
#define INDEXFILESIZE (sizeof(INDEXFILE) - 1)
#define SAMPLEHEADER "T\0\0\0\0\0\0"
#define HDRSIZE sizeof(SAMPLEHEADER)

struct tindexrecord {
    time_t mtime;
    short namelen;
    short filecnt;
    short dircnt;
    size_t filesize;
    size_t dirsize;
};

struct rindexrecord {
    short namelen;
    time_t mtime;
    uint64_t wkey[KEYSIZE]; /* wdir   key */
    uint64_t skey[KEYSIZE]; /* stage  key */
    uint64_t ckey[KEYSIZE]; /* commit key */
};

#endif
