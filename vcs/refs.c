#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "meta.h"

int createreference(char *name,uint64_t key[KEYSIZE]) {
    int error = 0;
    int fl = strlen(name);
    char fn[REFPATHSIZE + fl + 1];
    memcpy(fn,REFPATH,REFPATHSIZE);
    memcpy(fn+REFPATHSIZE,name,fl+1);
    FILE *ref = fopen(fn,"w");
    if (!ref) return -1;
    fwrite(key,sizeof(uint64_t)*KEYSIZE,1,ref);
    fclose(ref);
    return error;
}
