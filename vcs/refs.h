#ifndef __REFS_
#define __REFS_

#include <stdint.h>
#include "meta.h"

int createreference(char *name,uint64_t key[KEYSIZE]);

#endif
