#include "createobject.h"
#include "objects.h"
#include <fts.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

#include "refs.h"
#include "meta.h"

#define WRITEFILEINFO()                                                        \
    memcpy(entrylist[listlast].key, key, KEYSIZE * sizeof(uint64_t));          \
    entrylist[listlast].name = (char *)malloc(node->fts_namelen);              \
    if (!entrylist[listlast].name) {                                           \
        listlast++;                                                            \
        error = -4;                                                            \
        goto FAIL;                                                             \
    }                                                                          \
    entrylist[listlast].namelen = node->fts_namelen;                           \
    memcpy(entrylist[listlast].name, node->fts_name, node->fts_namelen);       \
    levellist[listlast] = node->fts_level;                                     \
    listlast++;

#define WRITEHDR(str,file) \
    fwr = fwrite((str),1,sizeof((str)),(file)); \
    if (fwr < sizeof((str))) {error = -2; goto FAIL;}


int compare(const FTSENT **one, const FTSENT **two) {
    if ((*one)->fts_level != (*two)->fts_level)
        return (((*one)->fts_level) - ((*two)->fts_level));
    if (((*one)->fts_info == FTS_D) ^ ((*two)->fts_info == FTS_D))
        return ((*one)->fts_info == FTS_D) ? +1 : -1;
    if (((*one)->fts_info == FTS_DP) ^ ((*two)->fts_info == FTS_DP))
        return ((*one)->fts_info == FTS_DP) ? -1 : 1;
    return (strcmp((*one)->fts_name, (*two)->fts_name));
}

/*
 * if (dir->dircnt==0 && dir->mtime == rindex->dir->mtime) {
 *     fts_set(dir,skip);
 *     seek(index+dsize);
 * }
 *
 *
 */

int processfiles(char *const argv[] /*null-terminated list of files*/,
                 int compr, struct entry *roottree) {
    if (!*argv) return 0;
    FTS *fs = fts_open(argv, FTS_NOCHDIR, &compare);
    if (!fs) return -1;
    int error = 0;
    unsigned int fwr = 0;
    uint32_t ecount = 0; /* entry count */
    
    struct entry *entrylist = NULL;
    short *levellist        = NULL;
    int listsize = 0, listlast = 0;
    
    FILE *index = NULL;
    index = fopen(INDEXFILE,"w");
    if (!index) {error = -1; goto FAIL;}
    WRITEHDR("idx",index)
    uint32_t null = 0;
    fwr = fwrite(&null,sizeof(uint32_t),1,index);
    if (fwr < 1) {error = -2; goto FAIL;}

    FTSENT *node = NULL;
    while ((node = fts_read(fs)) != NULL) {
        {
            char *p = node->fts_path;
            if (p) {
                if (p[0] == '.' && p[1] == '/') p += 2;
                if (node->fts_pathlen - 2 >= 9 && !strncmp(p, ".metadata", 9)) {
                    (void)fts_set(fs,node,FTS_SKIP);
                    continue;
                }
            }
        }
        switch (node->fts_info) {
            case FTS_D: {
                ecount++;
                int cnf    = 0;
                size_t fsz = 0;
                int cnd    = 0;
                size_t dsz = 0; (void)dsz;
                int cnt    = 0;
                FTSENT *ch = fts_children(fs, 0 /*,FTS_NAMEONLY*/);
                if (!ch) break;
                do {
                    cnt++;
                    if (ch->fts_info == FTS_D) cnd++;
                    else if (ch->fts_info == FTS_F) {cnf++;fsz+=sizeof(struct rindexrecord)+ch->fts_namelen;}
                } while ((ch = ch->fts_link) != NULL);

                WRITEHDR("dir",index)
                struct tindexrecord t;
                t.namelen = node->fts_namelen;
                t.filecnt = cnf;
                t.filesize = fsz;
                t.dircnt = cnd;
                t.dirsize = 0x0505050505050505;
                t.mtime = node->fts_statp->st_mtim.tv_sec;
                fwr = fwrite(&t,sizeof(struct tindexrecord),1,index);
                if (fwr < 1) {error = -2; goto FAIL;}
                fwr = fwrite(node->fts_name,1,node->fts_namelen,index);
                if (fwr < node->fts_namelen) {error = -2; goto FAIL;}
                printf("cd to dir: %s(%d,%d,%d)\n", node->fts_accpath, cnd, cnf, cnt);
            } break;
            case FTS_DOT: break;
            case FTS_F: {
                ecount++;
                uint64_t key[KEYSIZE] = {0};
                error = createrecordobject(compr, true, node->fts_accpath,
                                           node->fts_statp->st_size, &key);
                if (error) goto FAIL;
                if (listsize == listlast) {
                    ++listsize;
                    entrylist = (struct entry *)realloc(entrylist, listsize * sizeof(struct entry));
                    levellist = (short *)realloc(levellist, listsize * sizeof(short));
                    if (!entrylist || !levellist) {
                        error = -4;
                        goto FAIL;
                    }
                }
                WRITEFILEINFO()

                WRITEHDR("rec",index)
                struct rindexrecord r;
                r.namelen = node->fts_namelen;
                r.mtime = node->fts_statp->st_mtim.tv_sec;
                memcpy(r.wkey,key,sizeof(uint64_t)*KEYSIZE);
                memcpy(r.skey,key,sizeof(uint64_t)*KEYSIZE);
                memset(r.ckey,'\0',sizeof(uint64_t)*KEYSIZE);
                fwr = fwrite(&r,sizeof(struct rindexrecord),1,index);
                if (fwr < 1) {error = -2; goto FAIL;}
                fwr = fwrite(node->fts_name,1,node->fts_namelen,index);
                if (fwr < node->fts_namelen) {error = -2; goto FAIL;}
            } break;
            case FTS_DP: {
                ecount ++;
                if (!listlast ||
                    (listlast > 0 &&
                     levellist[listlast - 1] <=
                       node->fts_level /* that means directory is empty */))
                    break;
                int i = 0;
                for (i = listlast - 1; i >= 0 && levellist[i] > node->fts_level; i--);
                i += 1;
                struct entry *et      = &(entrylist[i]);
                uint64_t key[KEYSIZE] = {0};
                error = createtreeobject(compr, true, et, listlast - i, &key);
                if (error) goto FAIL;
                for (int j = listlast - 1; j >= i; j--)
                    free(entrylist[j].name); /*empty list and create tree
                                                reference in object list*/
                listlast = i;
                WRITEFILEINFO()

                WRITEHDR("eod",index)
                fwr = fwrite(key,sizeof(uint64_t)*KEYSIZE,1,index);
                if (fwr < 1) {error = -2; goto FAIL;}
            } break;
            default: break;
        }
    }
FAIL:
    if (!error && roottree && entrylist)
        *roottree = entrylist[0];
    else {
        if (entrylist)
            for (int i = 0; i < listlast; i++)
                free(entrylist[i].name);
    }
    free(entrylist);
    free(levellist);
    fts_close(fs);
    if (!error && index) {
        (void)fseek(index,sizeof(uint32_t),SEEK_SET);
        fwr = fwrite(&ecount,sizeof(uint32_t),1,index);
        if (fwr < 1) {error = -2;}
    }
    fclose(index);
    return error;
}

#undef WRITEFILEINFO
#undef WRITEHDR

int main(int argc, char *argv[]) {
    (void)argc;
    printf("sizeof(rir) = %d; szof(tir) = %d\n",sizeof(struct rindexrecord),sizeof(struct tindexrecord));
    struct entry e;
    e.name = NULL;
    int compr = 2;
    int i     = processfiles(argv + 1, compr, &e);
    (void)createreference("root", e.key);
    //(void)createcommitobject(compr,true,&e,0,NULL,0,NULL,NULL);
    free(e.name);
    return i;
}
