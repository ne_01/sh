#ifndef __OBJECTS__
#define __OBJECTS__

#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include "tiger/tiger.h"

struct entry {
    uint64_t key[KEYSIZE];
    size_t namelen;
    char *name;
};

struct tree {
    size_t size;
    struct entry *list;
};

struct record {
    void *data;
};

struct commit {
    time_t time;
    uint64_t *treeref[KEYSIZE];
    size_t parents;
    uint64_t (*parentkeys)[KEYSIZE];
    size_t csize;
    void *content;
};

struct object {
    unsigned char parsed:1;
    unsigned char type:2;
    uint64_t key[KEYSIZE];
    union {
        struct tree tree;
        struct record record;
        struct commit commit;
    } ref;
};

#endif
