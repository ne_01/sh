#ifndef __CR_OBJ_
#define __CR_OBJ_

#include "objects.h"

int createrecordobject(int, bool, char *, size_t, uint64_t (*)[KEYSIZE]);
int createtreeobject(int, bool, struct entry *, unsigned int, uint64_t (*)[KEYSIZE]);
int createcommitobject(int, bool, struct entry *, size_t, uint64_t (*)[KEYSIZE], size_t, void *, uint64_t (*)[KEYSIZE]);

#endif
