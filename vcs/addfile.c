#include "createobject.h"
#include "objects.h"
#include <fts.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "refs.h"

#define WRITEFILEINFO()                                                        \
    memcpy(entrylist[listlast].key, key, KEYSIZE * sizeof(uint64_t));          \
    entrylist[listlast].name = (char *)malloc(node->fts_namelen);              \
    if (!entrylist[listlast].name) {                                           \
        listlast++;                                                            \
        error = -4;                                                            \
        goto FAIL;                                                             \
    }                                                                          \
    entrylist[listlast].namelen = node->fts_namelen;                           \
    memcpy(entrylist[listlast].name, node->fts_name, node->fts_namelen);       \
    levellist[listlast] = node->fts_level;                                     \
    listlast++;

int compare(const FTSENT **one, const FTSENT **two) {
    if ((*one)->fts_level != (*two)->fts_level)
        return (((*one)->fts_level) - ((*two)->fts_level));
    if (((*one)->fts_info == FTS_D) ^ ((*two)->fts_info == FTS_D))
        return ((*one)->fts_info == FTS_D) ? +1 : -1;
    if (((*one)->fts_info == FTS_DP) ^ ((*two)->fts_info == FTS_DP))
        return ((*one)->fts_info == FTS_DP) ? -1 : 1;
    return (strcmp((*one)->fts_name, (*two)->fts_name));
}

int processfiles(char *const argv[] /*null-terminated list of files*/,
                 int compr, struct entry *roottree) {
    if (!*argv) return 0;
    FTS *fs = fts_open(argv, FTS_COMFOLLOW | FTS_NOCHDIR, &compare);
    if (!fs) return -1;
    int error = 0;

    struct entry *entrylist = NULL;
    short *levellist        = NULL;
    int listsize = 0, listlast = 0;

    FTSENT *node = NULL;
    while ((node = fts_read(fs)) != NULL) {
        {
            char *p = node->fts_path;
            if (p) {
                if (p[0] == '.' && p[1] == '/') p += 2;
                if (node->fts_pathlen - 2 > 9 && !strncmp(p, ".metadata", 9))
                    continue;
            }
        }
        switch (node->fts_info) {
            case FTS_D:
                printf("cd to dir: %s\n", node->fts_accpath); // break;
            case FTS_DOT: break;
            case FTS_F:
            case FTS_SL: {
                uint64_t key[KEYSIZE] = {0};
                error = createrecordobject(compr, true, node->fts_accpath,
                                           node->fts_statp->st_size, &key);
                if (error) goto FAIL;
                if (listsize == listlast) {
                    ++listsize;
                    entrylist = (struct entry *)realloc(entrylist, listsize * sizeof(struct entry));
                    levellist = (short *)realloc(levellist, listsize * sizeof(short));
                    if (!entrylist || !levellist) {
                        error = -4;
                        goto FAIL;
                    }
                }
                WRITEFILEINFO()
            } break;
            case FTS_DP: {
                if (!strcmp(node->fts_path, ".metadata")) break;
                if (!listlast ||
                    (listlast > 0 &&
                     levellist[listlast - 1] <=
                       node->fts_level /* that means directory is empty */))
                    break;
                int i = 0;
                for (i = listlast - 1; i >= 0 && levellist[i] > node->fts_level; i--);
                i += 1;
                struct entry *et      = &(entrylist[i]);
                uint64_t key[KEYSIZE] = {0};
                error = createtreeobject(compr, true, et, listlast - i, &key);
                if (error) goto FAIL;
                for (int j = listlast - 1; j >= i; j--)
                    free(entrylist[j].name); /*empty list and create tree
                                                reference in object list*/
                listlast = i;
                WRITEFILEINFO()
            } break;
            default: break;
        }
    }
FAIL:
    if (!error && roottree && entrylist)
        *roottree = entrylist[0];
    else {
        if (entrylist)
            for (int i = 0; i < listlast; i++)
                free(entrylist[i].name);
    }
    free(entrylist);
    free(levellist);
    fts_close(fs);
    return error;
}

#undef WRITEFILEINFO

int main(int argc, char *argv[]) {
    struct entry e;
    int compr = 2;
    int i     = processfiles(argv + 1, compr, &e);
    (void)createreference("root", e.key);
    //(void)createcommitobject(compr,true,&e,0,NULL,0,NULL,NULL);
    free(e.name);
    return i;
}
