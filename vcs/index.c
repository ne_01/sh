#include "createobject.h"
#include "meta.h"
#include "objects.h"
#include <fts.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#define ckintegrity(f) \
    if (!(f)) { \
        error = -2; \
        goto FAIL; \
    }

int updateindex() {
    int error       = 0;
    short *nlengths = NULL;
    int nll = 0, nlc = 0;
    char *path    = NULL;
    short pathlen = 0, pathmax = 0;

    FILE *findex = fopen(INDEXFILE, "r+");
    if (!findex) return -1;
    uint32_t sign[2] = {0};
    size_t sr        = fread(sign, sizeof(uint32_t), 2, findex);
    ckintegrity(sr>=2);
    ckintegrity(!strncmp((char *)sign, "idx", 3));

    //printf("r: %zu,%s,%d\n", sr, (char *)sign, sign[1]);
    while (sign[1]--) {
        uint32_t fsign;
        sr = fread(&fsign, sizeof(uint32_t), 1, findex);
        ckintegrity(sr >= 1);
        switch (fsign) {
            case '\0rid': {
                unsigned int rsz = sizeof(struct tindexrecord);
                struct tindexrecord r;
                sr = fread(&r, rsz, 1, findex);
                ckintegrity(sr >= 1);
                if (nll == nlc) {
                    nll      = nll ? (nll << 1) : 1;
                    nlengths = (short *)realloc(nlengths, nll * sizeof(short));
                    if (!nlengths) {
                        error = -1;
                        goto FAIL;
                    }
                }
                int pl_old      = pathlen;
                nlengths[nlc++] = r.namelen + sizeof(char);
                pathlen += nlengths[nlc - 1];
                if (pathlen + 1 > pathmax || !path) {
                    pathmax = pathlen + 1 /*for '\0'*/;
                    path    = (char *)realloc(path, pathmax * sizeof(char));
                    if (!path) {
                        error = -1;
                        goto FAIL;
                    }
                }
                sr = fread(path + pl_old, 1, r.namelen, findex);
                ckintegrity(sr >= (size_t)r.namelen);
                path[pathlen - 1] = '/';
                path[pathlen]     = '\0';

                /* actions */

                printf("dirpath:%s\n", path);
            } break;
            case '\0cer': {
                ckintegrity(nlengths && path);
                /* if we read a file entry but haven't read a dir entry yet,
                 * then index file is corrupted */
                unsigned int rsz = sizeof(struct rindexrecord);
                struct rindexrecord r;
                sr = fread(&r, rsz, 1, findex);
                ckintegrity(sr >= 1);

                int pl_old = pathlen;
                pathlen += r.namelen;
                if (pathlen + 1 > pathmax) {
                    pathmax = pathlen + 1 /*for '\0'*/;
                    path    = (char *)realloc(path, pathmax * sizeof(char));
                    if (!path) {
                        error = -1;
                        goto FAIL;
                    }
                }
                sr = fread(path + pl_old, 1, r.namelen, findex);
                ckintegrity(sr >= (size_t)r.namelen);
                path[pathlen] = '\0';
                printf("recpath:%s\n", path);

                /* actions */
                
                pathlen -= r.namelen;
            } break;
            case '\0doe': {
                ckintegrity(nlengths && path && nlc);
                /* if we read a enddir entry but haven't read a dir entry yet,
                 * then index file is corrupted */
                --nlc;
                pathlen -= nlengths[nlc];
                printf("pl = %d\n", pathlen);
                uint64_t key[KEYSIZE];
                sr = fread(&key, sizeof(uint64_t) * KEYSIZE, 1, findex);
                ckintegrity(sr >= 1);

                /* actions */
            
            } break;
            default: error = -3; goto FAIL;
        }
    }
FAIL:
    free(nlengths);
    free(path);
    fclose(findex);
    return error;
}

int main() {
    int i = updateindex();
    return i;
}
