#ifndef __TIGER__
#define __TIGER__
#include <stdint.h>

#define KEYSIZE 3

void tiger(void *, uint64_t, uint64_t res[KEYSIZE]);

#endif
