#include "createobject.h"
#include "base64.h"
#include "bzlib.h"
#include "meta.h"
#include "objects.h"
#include "tiger/tiger.h"
#include <errno.h>
#include <fts.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

#define MKHEADER(file, ch)                                                     \
    /* generate header */                                                      \
    memcpy(file, SAMPLEHEADER, HDRSIZE);                                       \
    ((uint8_t *)(file))[0] = (ch);

#define MKENTRYOBJECT(file, fsz)                                               \
    uint64_t key[KEYSIZE];                                                     \
    tiger((file), (fsz), key);

#define CHECKANDMAKEFILE(file, fsz)                                            \
    char fn[OBJPATHSIZE + KEYSIZE * sizeof(uint64_t) / 3 * 4 + 1];             \
    memcpy(fn, OBJPATH, OBJPATHSIZE);                                          \
    Base64encode(fn + OBJPATHSIZE, (char *)key, KEYSIZE * sizeof(uint64_t));   \
    fn[OBJPATHSIZE + KEYSIZE * sizeof(uint64_t) / 3 * 4] = '\0';               \
    /* check if file exists */                                                 \
    struct stat ef;                                                            \
    error = stat(fn, &ef) ? -5 : 0;                                            \
    if (!error || errno != ENOENT) {                                           \
        goto FAIL;                                                             \
    } else                                                                     \
        error = 0;                                                             \
    /* open file */                                                            \
    FILE *outf = fopen(fn, "w");                                               \
    if (!outf) {                                                               \
        error = -3;                                                            \
        goto FAIL;                                                             \
    }                                                                          \
    /* compress file */                                                        \
    int bzerror;                                                               \
    BZFILE *b = BZ2_bzWriteOpen(&bzerror, outf, compr, 0, 0);                  \
    if (bzerror != BZ_OK) {}                                                   \
    BZ2_bzWrite(&bzerror, b, (file), (fsz));                                   \
    if (bzerror != BZ_OK) {}                                                   \
    unsigned int lin, hin, lout, hout;                                         \
    BZ2_bzWriteClose64(&bzerror, b, 0, &lin, &hin, &lout, &hout);              \
    fclose(outf);                                                              \
    uint64_t in  = ((uint64_t)hin << 32) + (uint64_t)lin;                      \
    uint64_t out = ((uint64_t)hout << 32) + (uint64_t)lout;                    \
    (void)out;                                                                 \
    if (bzerror != BZ_OK) {}                                                   \
    if (in != (fsz)) {}                                                        \
// fwrite(file,d_size,1,outf);

int createrecordobject(int compr, bool mkfile, char *accpath, size_t st_size,
                       uint64_t (*pkey)[KEYSIZE]) {
    int error     = 0;
    size_t fsz    = st_size * sizeof(uint8_t) + HDRSIZE;
    uint8_t *file = (uint8_t *)malloc(fsz);
    if (!file) {
        error = -2;
        goto FAIL;
    }
    MKHEADER(file, 'r')
    /* add data */
    FILE *fptr = fopen(accpath, "r");
    if (!fptr) {
        error = -3;
        goto FAIL;
    }
    size_t c = HDRSIZE;
    while ((c += fread(file + HDRSIZE, 1, st_size * sizeof(uint8_t), fptr)) < fsz);
    fclose(fptr);

    MKENTRYOBJECT(file, fsz)
    if (mkfile) { CHECKANDMAKEFILE(file, fsz) }
FAIL:
    free(file);
    if (!error && pkey) memcpy(pkey, key, KEYSIZE * sizeof(uint64_t));
    return error;
}

int createtreeobject(int compr, bool mkfile, struct entry *et, unsigned int etlen,
                     uint64_t (*pkey)[KEYSIZE]) {
    int error       = 0;
    size_t treesize = HDRSIZE;
    for (unsigned int j = 0; j < etlen; j++)
        treesize += sizeof(uint64_t) * KEYSIZE + sizeof(short) + et[j].namelen;
    /*                           hash             namelen           name     */
    void *values = malloc(treesize);
    if (!values) {
        error = -2;
        goto FAIL;
    }
    MKHEADER(values, 't')
    /* add data */
    void *v = values + HDRSIZE;
    for (unsigned int j = 0; j < etlen; j++) {
        memcpy(v, et[j].key, sizeof(uint64_t) * KEYSIZE);
        v += sizeof(uint64_t) * KEYSIZE;
        memcpy(v, &et[j].namelen, sizeof(short));
        v += sizeof(short);
        memcpy(v, et[j].name, et[j].namelen);
        v += et[j].namelen;
    }
    MKENTRYOBJECT(values, treesize)
    if (mkfile) { CHECKANDMAKEFILE(values, treesize) }
FAIL:
    free(values);
    if (!error && pkey) memcpy(pkey, key, KEYSIZE * sizeof(uint64_t));
    return error;
}

int createcommitobject(int compr, bool mkfile, struct entry *e, size_t pcount,
                       uint64_t (*parents)[KEYSIZE], size_t msglen, void *msg,
                       uint64_t (*pkey)[KEYSIZE]) {
    int error      = 0;
    size_t objsize = HDRSIZE + sizeof(time_t) +            /*timestamp*/
                     sizeof(uint64_t) * KEYSIZE +          /*tree*/
                     sizeof(size_t) +                      /*pcount*/
                     sizeof(uint64_t) * KEYSIZE * pcount + /*parents*/
                     sizeof(size_t) +                      /*message length*/
                     msglen;
    void *commit = malloc(objsize);
    if (!commit) {
        error = -2;
        goto FAIL;
    }
    MKHEADER(commit, 'c')
    void *c   = commit + HDRSIZE;
    time_t ct = time(NULL);
    memcpy(c, &ct, sizeof(time_t));
    c += sizeof(time_t);
    memcpy(c, e->key, sizeof(uint64_t) * KEYSIZE);
    c += sizeof(uint64_t) * KEYSIZE;

    memcpy(c, &pcount, sizeof(size_t));
    c += sizeof(size_t);
    for (unsigned int i = 0; i < pcount; i++) {
        memcpy(c, parents[i], sizeof(uint64_t) * KEYSIZE);
        c += sizeof(uint64_t) * KEYSIZE;
    }
    memcpy(c, &msglen, sizeof(size_t));
    c += sizeof(size_t);
    memcpy(c, msg, msglen);
    /* c += msglen; // unnecessary */
    MKENTRYOBJECT(commit, objsize)
    if (mkfile) { CHECKANDMAKEFILE(commit, objsize) }
FAIL:
    free(commit);
    if (!error && pkey) memcpy(pkey, key, KEYSIZE * sizeof(uint64_t));
    return error;
}

#undef MKENTRYOBJECT
#undef CHECKANDMAKEEFILE
