//#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "objstorage.h"

static int comparekeys(const uint64_t key1[KEYSIZE], const uint64_t key2[KEYSIZE]) {
    for (char i=0;i<KEYSIZE;i++) if (key1[i] != key2[i]) return key1[i]-key2[i];
    return 0;
}

static int getheight(struct node *root) {
    if (!root) return 0;
    else return root->height;
}

static int max(int a,int b) {return (a>b)?a:b;}

static struct node *lrotate(struct node *root) {
    if (!root) return NULL;
    struct node *right = root->right;
    struct node *left = right->left;
    right->left = root; // perform rotation
    root->right = left;
    root->height = max(getheight(root->left), getheight(root->right)) + 1; // update heights of the rotated nodes
    right->height = max(getheight(right->left), getheight(right->right)) + 1;
    return right;
}

static struct node *rrotate(struct node *root) {
    struct node *left = root->left;
    struct node *right = left->right;
    left->right = root; // perform rotation
    root->left = right;
    root->height = max(getheight(root->left), getheight(root->right)) + 1; // update heights of the rotated nodes
    left->height = max(getheight(left->left), getheight(left->right)) + 1;
    return left;
}

static struct node *inserttree(struct node *root, uint64_t key[KEYSIZE], struct object value) {
    // normal insertion
    if (!root) {
        struct node *new_node = (struct node *)malloc(sizeof(struct node));
        if (!new_node) return NULL;
        new_node->left = NULL;
        new_node->right = NULL;
        memcpy(new_node->key,key,sizeof(uint64_t)*KEYSIZE);
        new_node->value = value;
        new_node->height = 1;
        return new_node;
    }
    register int r=comparekeys(root->key,key);
    if (r>0) root->left = inserttree(root->left, key, value);
    /* if key equal ... */
    else root->right = inserttree(root->right, key, value);
    root->height = max(getheight(root->left), getheight(root->right)) + 1;
    int balance = root?(getheight(root->left) - getheight(root->right)):0;
    if (balance > 1 && r>0) root = rrotate(root);// left-left case
    else if (balance < -1 && r<0) root = lrotate(root);// right-right case
    else if (balance > 1 && r<0) {// left-right case
        root->left = lrotate(root->left);
        root = rrotate(root);
    } else if (balance < -1 && r>0) { // right-left case
        root->right = rrotate(root->right);
        root = lrotate(root);
    }
    return root;
}

static struct node *balancetree(struct node *root) {
    struct node *x, *y;
    int lheight, rheight;
    lheight = getheight(root->left);
    rheight = getheight(root->right);
    x = (lheight >= rheight)?root->left:root->right;
    lheight = getheight(x->left);
    rheight = getheight(x->right);
    if (x == root->left) y = (lheight >= rheight)?x->left:x->right;
    if (x == root->right) {
        y = (lheight > rheight)?x->left:x->right;
    }
    if (root->left == x && x->left == y) root = rrotate(root); // left-left case
    else if (x == root->right && x->right == y) root = lrotate(root); // right-right case
    else if (x == root->left && y == x->right) { // left-right case
        root->left = lrotate(root->left);
        root = rrotate(root);
    }
    else if (x == root->right && y == x->left) { // right-left case
        root->right = rrotate(root->right);
        root = lrotate(root);
    }
    return root;
}

static struct node *deletetree(struct node *root, uint64_t key[KEYSIZE]) {
    if (!root) return NULL;
    int r=comparekeys(root->key,key);
    if (r>0) root->left = deletetree (root->left, key);
    else if (r<0) root->right = deletetree (root->right, key);
    else {
        struct node *temp;
        if (root->left == NULL || root->right == NULL) {
            if (root->left) temp = root->left;
            else if (root->right) temp = root->right;
            else temp = NULL;
            free(root);
            return temp;
        } else {
            temp = root->right;
            while (temp->left) temp = temp->left;
            memcpy(root->key,temp->key,sizeof(uint64_t)*KEYSIZE);
            root->value = temp->value;
            root->right = deletetree (root->right, temp->key);
        }
    }
    if (root) { // update height
        root->height = max(getheight(root->left), getheight(root->right)) + 1;
        int balance = root?(getheight(root->left) - getheight(root->right)):0;
        if (balance > 1 || balance < -1) root = balancetree(root);
    }
    return root;
}

static void destroy(struct node *root) {
    if (!root) return;
    if (root->left) destroy(root->left);
    if (root->right) destroy(root->right);
    free(root);
}

struct node *locatetree(struct node* root, uint64_t key[KEYSIZE]) {
    if (!root) return NULL;
    int r=comparekeys(root->key,key);
    if (r>0 && root->left) return locatetree(root->left,key);
    else if (r<0 && root->right) return locatetree(root->right,key);
    else if (r==0) return root;
    else return NULL;
}

/* } avl behaviour */

struct node *objstorage[SIZE];

uint32_t gethash(uint64_t key[KEYSIZE]) {
    uint64_t s=0;
    for (char i=0;i<KEYSIZE;i++) s+=key[i];
    return s%SIZE;
}
struct object *locate(uint64_t key[KEYSIZE]) {
    uint32_t index = gethash(key);
    struct node *rt=locatetree(objstorage[index],key);
    if (rt) return rt->value;
    else return NULL; 
}

void insert(uint64_t key[KEYSIZE],struct object value) {
    int index = gethash(key);
    objstorage[index]=inserttree(objstorage[index],key,value); 
}

void delete(uint64_t key[KEYSIZE]) {
    int index = gethash(key);
    objstorage[index]=deletetree(objstorage[index],key); 
}

