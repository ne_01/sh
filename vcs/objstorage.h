#ifndef __OBJSTORAGE__
#define __OBJSTORAGE__
#include <stdint.h>
#include "tiger/tiger.h"
#include "objects.h"

#define SIZE 64

/* avl behaviour { */
struct node {
    uint64_t key[KEYSIZE];
    //int value; /* tmp */
    struct object *value;
    struct node *left;
    struct node *right;
    int height;
};

extern struct node *objstorage[SIZE];
int /* tmp */ locate(uint64_t key[KEYSIZE]);

void insert(uint64_t key[KEYSIZE], int /* tmp */ value);
void delete(uint64_t key[KEYSIZE]);
#endif
