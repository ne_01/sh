#include <stdio.h>
#include <string.h>

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include <dirent.h>
#include <fts.h>
#include <errno.h>

#include <sys/stat.h>

int lua_mkpath(lua_State *L) {
    char *path, *slash;
    slash = path = (char *)luaL_checkstring(L, 1);
    struct stat sb;
    int done = 0;

    while (!done) {
        slash += strspn(slash, "/");
        slash += strcspn(slash, "/");

        done   = (*slash == '\0');
        *slash = '\0';

        if (stat(path, &sb)) {
            if (errno != ENOENT || (mkdir(path, 0777) && errno != EEXIST)) {
                lua_pushnil(L);
                lua_pushfstring(L, "%s: %s", path, strerror(errno));
                return 2;
            }
        } else if (!S_ISDIR(sb.st_mode)) {
            lua_pushnil(L);
            lua_pushfstring(L, "%s: %s", path, strerror(ENOTDIR));
            return 2;
        }
        *slash = '/';
    }
    lua_pushboolean(L, 1);
    return 1;
}

/* stat supplementary functions { */

static const char *mode2string(mode_t mode) {
    if (S_ISREG(mode))       return "file";
    else if (S_ISDIR(mode))  return "directory";
    else if (S_ISLNK(mode))  return "link";
    else if (S_ISSOCK(mode)) return "socket";
    else if (S_ISFIFO(mode)) return "named pipe";
    else if (S_ISCHR(mode))  return "char device";
    else if (S_ISBLK(mode))  return "block device";
    else                     return "other";
}
static const char *perm2string(mode_t mode) {
    static char perms[10] = "---------";
    if (mode & S_IRUSR) perms[0] = 'r';
    if (mode & S_IWUSR) perms[1] = 'w';
    if (mode & S_IXUSR) perms[2] = 'x';
    if (mode & S_IRGRP) perms[3] = 'r';
    if (mode & S_IWGRP) perms[4] = 'w';
    if (mode & S_IXGRP) perms[5] = 'x';
    if (mode & S_IROTH) perms[6] = 'r';
    if (mode & S_IWOTH) perms[7] = 'w';
    if (mode & S_IXOTH) perms[8] = 'x';
    return perms;
}

/* inode protection mode */
static void push_st_mode(lua_State *L, struct stat *info) {
    lua_pushstring(L, mode2string(info->st_mode));
}
/* device inode resides on */
static void push_st_dev(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_dev);
}
/* inode's number */
static void push_st_ino(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_ino);
}
/* number of hard links to the file */
static void push_st_nlink(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_nlink);
}
/* user-id of owner */
static void push_st_uid(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_uid);
}
/* group-id of owner */
static void push_st_gid(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_gid);
}
/* device type, for special file inode */
static void push_st_rdev(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_rdev);
}
/* time of last access */
static void push_st_atime(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_atime);
}
/* time of last data modification */
static void push_st_mtime(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_mtime);
}
/* time of last file status change */
static void push_st_ctime(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_ctime);
}
/* file size, in bytes */
static void push_st_size(lua_State *L, struct stat *info) {
    lua_pushinteger(L, (lua_Integer)info->st_size);
}
static void push_st_perm(lua_State *L, struct stat *info) {
    lua_pushstring(L, perm2string(info->st_mode));
}

const char *const stat_fields[] = {
    "mode",
    "dev",
    "ino",
    "nlink",
    "uid",
    "gid",
    "rdev",
    "atime",
    "mtime",
    "ctime",
    "size",
    "permissions",
    NULL
};

typedef void (*stat_func)(lua_State *, struct stat *);

stat_func stat_funcs[] = {
    push_st_mode,
    push_st_dev,
    push_st_ino,
    push_st_nlink,
    push_st_uid,
    push_st_gid,
    push_st_rdev,
    push_st_atime,
    push_st_mtime,
    push_st_ctime,
    push_st_size,
    push_st_perm,
    NULL
};

int stat_mt(lua_State *L) {
    struct stat *s = *(struct stat **)luaL_checkudata(L, 1, "struct stat *"); 
    int opt = luaL_checkoption(L, 2, NULL, stat_fields); 
    lua_pop(L,2);
    stat_func optf = stat_funcs[opt];
    optf(L,s);
    return 1;
}

/* } stat supplementary functions */

static const char *info2string(unsigned short info) {
    switch (info) {
        case FTS_D: return "directory";
        case FTS_DC: return "cycled directory";
        case FTS_DEFAULT: return "default";
        case FTS_DNR: return "non-readable directory";
        case FTS_DOT: return "dot directory";
        case FTS_DP: return "visited directory";
        case FTS_ERR: return "error";
        case FTS_F: return "file";
        case FTS_NS: return "file without stat";
        case FTS_NSOK: return "file no-stat";
        case FTS_SL: return "symbolic link";
        case FTS_SLNONE: return "symbolic link to non-existent file";
        default: return "other";
    }
}

static void push_info(lua_State *L, FTSENT *node) {
    lua_pushstring(L, info2string(node->fts_info));
}
static void push_accpath(lua_State *L, FTSENT *node) {
    lua_pushstring(L, node->fts_accpath);
}
static void push_path(lua_State *L, FTSENT *node) {
    lua_pushstring(L, node->fts_path);
}
static void push_name(lua_State *L, FTSENT *node) {
    lua_pushstring(L, node->fts_name);
}
static void push_level(lua_State *L, FTSENT *node) {
    lua_pushinteger(L, (lua_Integer)node->fts_level);
}
static void push_error(lua_State *L, FTSENT *node) {
    lua_pushstring(L, strerror(node->fts_errno));
}
static void push_parent(lua_State *L, FTSENT *node) {
    if (node->fts_parent) {
        FTSENT **n = (FTSENT **)lua_newuserdata(L, sizeof(FTSENT *));
        *n = node->fts_parent;
        luaL_setmetatable(L, "FTSENT*");
    } else lua_pushnil(L);
}
static void push_link(lua_State *L, FTSENT *node) {
    if (node->fts_link) {
        FTSENT **n = (FTSENT **)lua_newuserdata(L, sizeof(FTSENT *));
        *n = node->fts_link;
        luaL_setmetatable(L, "FTSENT*");
    } else lua_pushnil(L);
}
static void push_cycle(lua_State *L, FTSENT *node) {
    if (node->fts_cycle) {
        FTSENT **n = (FTSENT **)lua_newuserdata(L, sizeof(FTSENT *));
        *n = node->fts_cycle;
        luaL_setmetatable(L, "FTSENT*");
    } else lua_pushnil(L);
}
static void push_stat(lua_State *L, FTSENT *node) {
    if (node->fts_statp) {
        struct stat **n = (struct stat **)lua_newuserdata(L, sizeof(struct stat *));
        *n = node->fts_statp;
        luaL_setmetatable(L, "struct stat *");
    } else lua_pushnil(L);
}


const char *const ftsent_fields[] = {
    "info",
    "accpath",
    "path",
    "name",
    "level",
    "error",
    "parent",
    "link",
    "cycle",
    "stat",
    NULL
};

typedef void (*ftsent_func)(lua_State *, FTSENT *);

ftsent_func ftsent_funcs[] = {
    push_info,
    push_accpath,
    push_path,
    push_name,
    push_level,
    push_error,
    push_parent,
    push_link,
    push_cycle,
    push_stat,
    NULL
};

static int ftsent_mt(lua_State *L) {
    FTSENT *s = *(FTSENT **)luaL_checkudata(L, 1, "FTSENT*"); 
    int opt = luaL_checkoption(L, 2, NULL, ftsent_fields); 
    lua_pop(L,2);
    ftsent_func optf = ftsent_funcs[opt];
    optf(L,s);
    return 1;
}

/* ls (list directory) */

static int dir_iter(lua_State *L) {
    DIR *d = *(DIR **)luaL_checkudata(L, lua_upvalueindex(1), "DIR*");
    struct dirent *entry = readdir(d);
    if (entry != NULL) {
        lua_pushstring(L, entry->d_name);
        return 1;
    } else return 0; /* no more values to return */
}

static int l_dir(lua_State *L) {
    const char *path = luaL_checkstring(L, 1);
    DIR **d = (DIR **)lua_newuserdata(L, sizeof(DIR *)); /* create a userdata to store a DIR address */
    *d = NULL;
    /* set its metatable */
    luaL_setmetatable(L, "DIR*");
    /* try to open the given directory */
    *d = opendir(path);
    if (*d == NULL) /* error opening the directory? */
        luaL_error(L, "cannot open %s: %d: %s", path, errno, strerror(errno));
    /* creates and returns the iterator function;
     its sole upvalue, the directory userdata,
     is already on the top of the stack */
    lua_pushcclosure(L, dir_iter, 1);
    return 1;
}

static int dir_gc(lua_State *L) {
    DIR *d = *(DIR **)lua_touserdata(L, 1);
    if (d) closedir(d);
    return 0;
}

/* recursively list directory */
static int compare(const FTSENT **one, const FTSENT **two) {
    if ((*one)->fts_level != (*two)->fts_level)
        return (((*one)->fts_level) - ((*two)->fts_level));
    if (((*one)->fts_info == FTS_D) ^ ((*two)->fts_info == FTS_D))
        return ((*one)->fts_info == FTS_D) ? +1 : -1;
    if (((*one)->fts_info == FTS_DP) ^ ((*two)->fts_info == FTS_DP))
        return ((*one)->fts_info == FTS_DP) ? -1 : 1;
    return (strcmp((*one)->fts_name, (*two)->fts_name));
}

static int fts_iter(lua_State *L) {
    FTS *d = *(FTS **)luaL_checkudata(L, lua_upvalueindex(1), "FTS*");
    FTSENT *node = fts_read(d);
    if (node != NULL) {
        FTSENT **n = (FTSENT **)lua_newuserdata(L, sizeof(FTSENT *));
        *n = node;
        luaL_setmetatable(L, "FTSENT*");
        return 1;
    } else return 0; /* no more values to return */
}

static int fts_dir(lua_State *L) {
    int n = lua_gettop(L);
    const char *paths[n+1];
    const char *path;
    for (int i=0;i<n;i++) {
        path=luaL_checkstring(L, 1);
        paths[i] = path;
    }
    paths[n]=NULL;
    FTS **d = (FTS **)lua_newuserdata(L, sizeof(FTS *)); /* create a userdata to store a FTS address */
    *d = NULL;
    luaL_setmetatable(L, "FTS*"); /* set its metatable */
    *d = fts_open((char *const *)paths, 0 /*FTS_NOCHDIR*/ , &compare); /* try to open the given directory */
    if (*d == NULL) /* error opening the directory? */
        luaL_error(L, "cannot open %s: %d: %s", path, errno, strerror(errno));
    /* creates and returns the iterator function;
     its sole upvalue, the directory userdata,
     is already on the top of the stack */
    lua_pushcclosure(L, fts_iter, 1);
    return 1;
}

static int fts_gc(lua_State *L) {
    FTS *d = *(FTS **)luaL_checkudata(L, 1, "FTS*");
    if (d) fts_close(d);
    return 0;
}

int luaopen_initlfs(lua_State *L) {
    /*ftsent_mt*/
    luaL_newmetatable(L, "FTSENT*");
    lua_pushcfunction(L, ftsent_mt);
    lua_setfield(L, -2, "__index");
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "__metatable");
    /*stat_mt*/
    luaL_newmetatable(L, "struct stat *");
    lua_pushcfunction(L, stat_mt);
    lua_setfield(L, -2, "__index");
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "__metatable");
    /*fts_list*/
    luaL_newmetatable(L, "FTS*");
    lua_pushcfunction(L, fts_gc); /* set its __gc field */
    lua_setfield(L, -2, "__gc");
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "__metatable");
    lua_register(L, "traverse", fts_dir);
    /*dirlist*/
    luaL_newmetatable(L, "DIR*");
    lua_pushcfunction(L, dir_gc); /* set its __gc field */
    lua_setfield(L, -2, "__gc");
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "__metatable");
    lua_register(L, "dirlist", l_dir);
    lua_register(L, "mkpath", lua_mkpath);
    return 4;
}

