#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include "lfs.h"
#include "luainit.h"
#include "ui.h"

static int lua_fexit(lua_State *L) {
    (void)L;
    exitp=1;
    return 0;
}

static int new_print(lua_State *L) {
  int n = lua_gettop(L);  /* number of arguments */
  int i;
  lua_getglobal(L, "tostring");
  for (i=1; i<=n; i++) {
    const char *s;
    size_t l;
    lua_pushvalue(L, -1);  /* function to be called */
    lua_pushvalue(L, i);   /* value to print */
    lua_call(L, 1, 1);
    s = lua_tolstring(L, -1, &l);  /* get result */
    if (s == NULL)
      return luaL_error(L, "'tostring' must return a string to 'print'");
    if (i>1) uprintf(0, "\t");
    uprintf(0, "%s", s);
    lua_pop(L, 1);  /* pop result */
  }
  uprintf(1, "\n");
  return 0;
}

static int luaopen_intlib(lua_State *L) {
    luaopen_initlfs(L);
    /*exit*/
    lua_register(L, "exit", lua_fexit);
    /*print*/lua_register(L, "print", new_print);
    return 1;
}

lua_State *gL = NULL;

int interpret(const char *const buf) {
    int error = luaL_dostring(gL, buf); 
    if (error) {
        uprintf(1, "%s\n", lua_tostring(gL, -1));
        lua_pop(gL, 1); /* pop error message from the stack */
    }
    return error;
}

int appendcodestr(const char *const buf) {
    int error = luaL_dostring(gL, buf); 
    if (error) {
        uprintf(1, "%s\n", lua_tostring(gL, -1));
        lua_pop(gL, 1); /* pop error message from the stack */
    }
    return error;
}

static void l_print(lua_State *L) {
    int n = lua_gettop(L);
    if (n > 0) {  /* any result to be printed? */
        luaL_checkstack(L, LUA_MINSTACK, "too many results to print");
        lua_getglobal(L, "print");
        lua_insert(L, 1);
        if (lua_pcall(L, n, 0, 0) != LUA_OK)
            uprintf(1, "error calling 'print' (%s)",lua_tostring(L, -1));
    }
}

int dotop() {
    lua_call(gL, 0, LUA_MULTRET);
}

#if 0
static int addreturn(lua_State *L, const char *const buf) {
  const char *retline = lua_pushfstring(L, "return %s;", buf);
  int status = luaL_loadbuffer(L, retline, strlen(retline), "=in");
  if (status == LUA_OK) {
      lua_remove(L, -2);  /* remove modified line */
  } else lua_pop(L, 2);  /* pop result from 'luaL_loadbuffer' and modified line */
  return status;
}
#endif

int initlua (void) {
    gL = luaL_newstate();
    if (!gL) return 3;
    luaL_openlibs(gL); /* opens the standard libraries */
    luaopen_intlib(gL);
    return 0;
}

int deinitlua (void) {
    lua_close(gL);
    return 0;
}
