CC=cc
CFLAGS=-Wall -Wextra -Wpedantic -Wno-pointer-arith -g
OBJ=lfs.o luainit.o ui.o main.o
VG=valgrind
VGARGS=-v --leak-resolution=high --show-leak-kinds=all --track-origins=yes --log-file=summary --leak-check=full --leak-check-heuristics=all
LDFLAGS+=-lm -lreadline -lncursesw
.PHONY: clean

a.out: $(OBJ)
	$(CC) $(OBJ) liblua.a $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf a.out *.o *.gcno *.gcda *.gcov

