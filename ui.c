#include <locale.h>
#include <ncurses.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

WINDOW *win, *win1;
unsigned char inc, avail = 0, act = 1, exitp = 0; /*input char*/
static int isavail() { return avail; }
int
  sh = 0; /*quick and dirty hack: this var contains y coord. of line at which
             current input line begins; it is needed for redisplay func. */

static int rlgetc(FILE *dummy) {
    (void)dummy;
    avail = 0;
    return inc;
}

void pchar(unsigned char c) {
    inc   = c;
    avail = 1;
    rl_callback_read_char();
}

static void redisplay(void) {
    int cy = -1, cx = -1;
    int pcy = -1, pcx = -1;
    int prev_col = 0;
    int height   = 1;
    wmove(win, sh, 0);
    for (int in = 0; rl_display_prompt && rl_display_prompt[in]; in++) {
        waddch(win, rl_display_prompt[in]);
        getyx(win, cy, cx);
        if (cx < prev_col) height++;
        prev_col = cx;
    }
    for (int in = 0; in < rl_end; in++) {
        if (in == rl_point) getyx(win, pcy, pcx);
        unsigned char c = (unsigned char)rl_line_buffer[in];
        if (CTRL_CHAR(c) || c == RUBOUT) {
            waddch(win, '^');
            waddch(win, CTRL_CHAR(c) ? UNCTRL(c) : '?');
        } else
            waddch(win, c);
        getyx(win, cy, cx);
        (void)cy;
        if (cx < prev_col) height++;
        prev_col = cx;
    }
    wclrtobot(win);
    getyx(win, sh, cx);
    if (pcy >= 0) wmove(win, pcy, pcx);
    sh -= height - 1;
    wrefresh(win);
}

static int shnl(int key, int c) {
    rl_set_prompt(">> ");
    rl_done = 1;
    return 0;
}

static void shandle(char *buf) {
    extern int interpret(const char *const);
    int my, mx;
    getmaxyx(win, my, mx);
    sh += (strlen(rl_prompt) + rl_end) / mx +
          (((strlen(rl_prompt) + rl_end) % mx) ? 1 : 0);
    if (sh >= my) {
        scroll(win);
        sh = my - 1;
        wmove(win, sh, 0);
    }
    wmove(win, sh, 0);
    rl_on_new_line();
    if (buf && *buf) {
        add_history(buf);
        int i = interpret(buf);
        if (!i) free(buf);
        rl_pending_input = 1;
    }
}

int uprintf(const char refr, const char *fmt, ...) {
    va_list vl;
    va_start(vl, fmt);
    int r;
    r = vw_printw(win, fmt, vl);
    if (refr) wrefresh(win);
    int cx;
    getyx(win, sh, cx);
    (void)cx;
    va_end(vl);
    return r;
}

char *completion_null(const char *n1, int n2) {
    (void)n1;
    (void)n2;
    return NULL;
}

void initrl() {
    rl_catch_signals                 = 0;
    rl_catch_sigwinch                = 0;
    rl_deprep_term_function          = NULL;
    rl_prep_term_function            = NULL;
    rl_attempted_completion_function = NULL;
    rl_attempted_completion_over     = 1;
    rl_completion_entry_function     = completion_null;
    rl_change_environment            = 0;
    rl_getc_function                 = rlgetc;
    rl_input_available_hook          = isavail;
    rl_redisplay_function            = redisplay;
    rl_callback_handler_install("\\> ", shandle);
    rl_bind_key('\n', shnl);
    int cy, cx;
    getyx(win, cy, cx);
    (void)cx;
    sh = cy;
}

void deinitrl() {
    rl_set_prompt((char *)NULL);
    rl_callback_handler_remove();
    rl_cleanup_after_signal();
    rl_free_line_state();
    rl_clear_history();
}

void initnc(void) {
    setlocale(LC_ALL, "");
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(2);
    refresh();
    int w = 80, h = 25;
    int sy = (LINES - h - 3); // 2;
    int sx = (COLS - w) / 2;
    win1   = newwin(h + 2, w + 2, sy + 1, sx + 1);
    win    = derwin(win1, h, w, 1, 1);
    box(win1, 0, 0);
    wrefresh(win1);
    wmove(win, 0, 0);
    scrollok(win, TRUE);
}

void deinitnc(void) {
    delwin(win);
    delwin(win1);
    echo();
    endwin();
}
